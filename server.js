require('dotenv').config()
const express = require('express');
const RouterManager = require("./routes");
const cors = require('cors');
const app = express();
const port = 4000;
const bodyparser = require('body-parser');
const db = require("./lib/sequelize");

app.use(cors())
app.use(bodyparser.json());

RouterManager(app);

db.sequelize.sync().then(() => {
  console.log(" re-sync db.");
});
// // ajout de socket.io
const server = require('http').Server(app)

const io = require('socket.io')(server, {
  cors: {
    origin: process.env.APP_URL,
    methods: ["GET", "POST"]
  }
});

// établissement de la connexion
io.on('connection', (socket) =>{
  console.log(`Connecté au client ${socket.id}`);
  socket.on('code', data => {
    io.emit('newContent', data)
  })
  //  https://socket.io/docs/v3/rooms/ <-- To join & send messageto specific room
})

server.listen(port, () => {
    console.log(`Listening at http://api.localhost:${port}`)
})