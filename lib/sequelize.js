const Sequelize = require("sequelize");

const sequelize = new Sequelize('postgres://node:node@db:5432/node');

sequelize.authenticate()
  .then(() => console.log("connected to PG"))
  .catch((error) => console.error(error));

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require("../models/sequelize/User")(sequelize, Sequelize);
db.group = require("../models/sequelize/Group")(sequelize, Sequelize);
db.project = require("../models/sequelize/Project")(sequelize, Sequelize);

db.user.belongsToMany(db.group, {
    through: "user_group",
    as: "groups",
    foreignKey: "user_id",
});
db.group.belongsToMany(db.user, {
    through: "user_group",
    as: "users",
    foreignKey: "group_id",
});
db.group.belongsTo(db.user, {
    foreignKey: 'createdBy',
});
db.project.belongsTo(db.group, {
    foreignKey: 'group_id',
});
db.project.belongsTo(db.user, {
    foreignKey: 'createdBy',
});
db.group.belongsToMany(db.project, {
    through: "group_project",
    as: "projects",
    foreignKey: "group_id",
});
module.exports = db;
