const express = require("express");
const db = require("../lib/sequelize");
const Project = db.project;
const router = express.Router();

router.get("/", (req, res) => {
  Project.findAll({
  })
      .then((data) => res.json(data))
      .catch((err) => res.sendStatus(500));
});

router.get("/:id", (req, res) => {
  Project.findByPk(req.params.id)
      .then((data) => (data ? res.json(data) : res.sendStatus(404)))
      .catch((err) => res.sendStatus(500));
});

// get groups of user ( resp )
router.get('/resp/:id', (req, res) => {
  Project.findAll({
        where: { createdBy: req.params.id }
      }
  ).then((data) => res.json(data))
      .catch((err) => err.errors);
})

router.post("/", (req, res) => {
  Project.create(req.body)
      .then((data) => res.status(201).json(data))
      .catch((err) => err.errors);
});

router.put("/:id", (req, res) => {
  Project.update(req.body, { where: { id: req.params.id }, returning: true })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
      .catch((err) => res.sendStatus(500));
});

router.delete("/:id", (req, res) => {
  Project.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
