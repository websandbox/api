const SecurityRouter = require("./security");
const UserRouter = require("./users");
const GroupRouter = require("./groups")
const ProjectRouter = require("./project")

const routerManager = (app) => {
    app.use("/", SecurityRouter);
    app.use("/users", UserRouter);
    app.use("/groups", GroupRouter)
    app.use("/projects", ProjectRouter)
};

module.exports = routerManager;
