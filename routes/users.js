const express = require("express");
const db = require("../lib/sequelize");
const Group = db.group;
const User = db.user;
const router = express.Router();

router.get("/", (req, res) => {
  User.findAll({
  })
    .then((data) => res.json(data))
      .catch((err) => res.sendStatus(500));
});

router.get("/:id", (req, res) => {
  User.findByPk(req.params.id)
    .then((data) => (data ? res.json(data) : res.sendStatus(404)))
      .catch((err) => res.sendStatus(500));
});

//get user + user's group
router.get("/groups/:id", (req, res) => {
  User.findByPk(req.params.id,{
    include: [
      {
        model: Group,
        as: "groups",
        attributes: ["id", "name"],
        through: {
          attributes: [],
        }
      }]
  })
      .then((data) => res.json(data))
      .catch((err) => res.sendStatus(500));
});


router.put("/:id", (req, res) => {
  User.update(req.body, { where: { id: req.params.id }, returning: true })
    .then(([nbUpdated, result]) =>
      nbUpdated ? res.json(result[0]) : res.sendStatus(404)
    )
      .catch((err) => res.sendStatus(500));
});

router.delete("/:id", (req, res) => {
  User.destroy({
    where: {
      id: req.params.id,
    },
  })
    .then((nbDelete) => (nbDelete ? res.sendStatus(204) : res.sendStatus(404)))
    .catch((err) => res.sendStatus(500));
});

module.exports = router;
