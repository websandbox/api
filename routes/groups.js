const express = require("express");
const db = require("../lib/sequelize");
const Group = db.group;
const User = db.user;

const router = express.Router();

router.get("/", (req, res) => {
    Group.findAll({
    })
        .then((data) => res.json(data))
        .catch((err) => res.sendStatus(500));
});
// (return group + group's users
router.get("/users/:id", (req, res) => {
    Group.findByPk(req.params.id,{
          include: [
              {
                  model: User,
                  as: "users",
                  attributes: ["id", "email"],
                  through: {
                      attributes: [],
                  }
              }]
    })
        .then((data) => res.json(data))
        .catch((err) => res.sendStatus(500));
});

// get groups of user ( resp )
router.get('/resp/:id', (req, res) => {
    Group.findAll({
        where: { createdBy: req.params.id }
      }
  ).then((data) => res.json(data))
      .catch((err) => err.errors);
})

router.post("/", (req, res) => {
    Group.create(req.body)
        .then((data) => res.status(201).json(data))
        .catch((err) => err.errors);
});

router.post('/add/:groupId/:userId', (req, res) => {
        return Group.findByPk(req.params.groupId)
            .then((group) => {
                if (!group) {
                    console.log("group not found!");
                    return null;
                }
                return User.findByPk(req.params.userId).then((User) => {
                    if (!User) {
                        console.log("User not found!");
                        return null;
                    }
                    group.addUser(User);
                    return User;
                });
            }).catch(error => {
                console.log(error)
            })
})


module.exports = router;
