const {  DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Project = sequelize.define("Project", {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: false,
        }
    });
    return Project;
};



