const { DataTypes } = require("sequelize");

module.exports = (sequelize, Sequelize) => {
    const Group = sequelize.define("Group", {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true,
        },
        capacity : {
            type: DataTypes.STRING,
            allowNull: false,
            defaultValue: 10,
        },
    });
    return Group;
};



